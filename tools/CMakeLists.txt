
# Version used during development
cmake_minimum_required(VERSION 3.20)
project(${PROJECT_NAME}/Tools)

# Include custom CMake modules and utilities
file(GLOB CMAKE_MODULE_PATHS "${CMAKE_CURRENT_SOURCE_DIR}/../cmake/*")
foreach(MODULE_PATH ${CMAKE_MODULE_PATHS})
    if(IS_DIRECTORY ${MODULE_PATH})
        list(PREPEND CMAKE_MODULE_PATH ${MODULE_PATH})
    endif()
endforeach()

include(MessageColor)
include(FindPackageStandard)
include(ProjectArchitecture)
include(ParentCMakeOnly)

#
# Looking for script structure
message_title("Tools")

FILE_SOURCES(TOOLS "${CMAKE_CURRENT_SOURCE_DIR}")
DUMP_ARCHITECTURE(TOOLS)
message("")

GET_COMMON_PATH(TOOLPATH_COMMON TOOLS)

# Add make command to run tools
add_custom_target(tools COMMENT "Running tools")

#
# Looking for script files
foreach( SRC ${TOOLS} )

    get_filename_component(TOOL ${SRC} NAME_WE)
    if(TOOL STREQUAL "main")
        get_filename_component(TOOL ${SRC} DIRECTORY)
        get_filename_component(TOOL ${TOOL} NAME_WE)
    endif()

    get_filename_component(TOOLPATH ${SRC} PATH)
    file(RELATIVE_PATH TOOLPATH_REL ${TOOLPATH_COMMON} ${TOOLPATH})
    if(NOT "${TOOLPATH_REL}" STREQUAL "")
        list(APPEND TOOLPATH_REL "/")
    endif()

    message_color("-- Preparing target tools `./${TOOLPATH_REL}${TOOL}`" COLOR GREEN)

    add_executable(${TOOL} ${SRC})
    target_link_package(${TOOL} ${LIBRARY} REQUIRED)
    
    add_dependencies(tools ${TOOL}) # Custom `make tools`
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${TOOL}
        DESTINATION bin
        PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
        OPTIONAL
    )

endforeach()
