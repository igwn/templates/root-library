/**
 **********************************************
 *
 * \file libMyRoot.LinkDef.h
 * \brief Link definitions of the library
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#ifdef __CINT__
#include "libMyRoot.Config.h"

#pragma link off all globals;
#pragma link off all functions;

#pragma link C++ namespace MyRoot+;

// Example of pImpl implementation of class
#pragma link C++ class MyRoot::MyClass<int>+;
#pragma link C++ class MyRoot::MyClass<float>+;
#pragma link C++ class MyRoot::MyClass<double>+;

#pragma link C++ class MyRoot::THistogram+;

#if CURL_FOUND
/* Put here classes depending on CURL */
#endif

#endif