#ifndef MYROOT_MYHISTOGRAM_HH
#define MYROOT_MYHISTOGRAM_HH

#include <Riostream.h>
#include <TH1D.h>

#include <libMyRoot.Config.h>
#if FOUND_CURL
/* Put here code that depends on CURL */
#endif

namespace MyRoot {
    class THistogram : public TH1D {
    public:
    
        THistogram() ;
        THistogram(const char* name, const char* title, Int_t nbins, Double_t xmin, Double_t xmax);
        virtual ~THistogram();

        Int_t Fill(double value);

        #if FOUND_CURL
        /* Put here code that depends on CURL */
        #endif

        ClassDef(THistogram, 1) // Required for ROOT dictionary
    };
} // namespace MyRoot

#endif // MYROOT_MYHISTOGRAM_HH