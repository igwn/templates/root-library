/**
 * @file THistogramTest.cpp
 * @brief A program demonstrating the usage of a custom histogram class with Doxygen comments.
 */

#include "MyRoot/impl/MyClass.h" // Include your THistogram header file

/**
 * @brief Main function to generate a histogram and test it.
 *  
 * @return 0 on successful execution.
 */
int main() {

    // Create an instance of your custom histogram class THistogram
    MyRoot::MyClass<double> *cl = new MyRoot::MyClass<double>({1,2,3});
                     cl->Print();

    delete cl;
    cl = NULL;

    return 0;
}