#include "MyRoot/impl/MyClassImpl.h"

namespace MyRoot {

    template <typename Element>
    void MyClass<Element>::Impl::Print(const std::vector<size_t>& indices) {
        std::cout << "MyClass(" << indices.size() << " indices)" << std::endl;

        for (auto idx : indices) {
            std::cout << "Index: " << idx << std::endl;
        }
    }

    // Explicit instantiations for MyClass::Impl::Print
    template class MyClass<int>::Impl;
    template class MyClass<float>::Impl;
    template class MyClass<double>::Impl;
}