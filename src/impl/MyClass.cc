#include "MyRoot/impl/MyClass.h"
#include "MyRoot/impl/MyClassImpl.h"

namespace MyRoot {

    template<typename Element>
    MyClass<Element>::MyClass() { pImpl = new MyClass<Element>::Impl(); }

    template<typename Element>
    MyClass<Element>::MyClass(const std::vector<size_t> &indices) : MyClass() 
    {
        this->indices = indices;
    }

    template<typename Element>
    void MyClass<Element>::Print()
    {
        pImpl->Print(indices);
    }

    template<typename Element>
    MyClass<Element>::~MyClass() {
        if (pImpl) {
            delete pImpl;
            pImpl = nullptr;
        }
    }

    // Explicit instantiations for MyClass
    template class MyClass<int>;
    template class MyClass<float>;
    template class MyClass<double>;
}