#include "MyRoot/THistogram.h"
ClassImp(MyRoot::THistogram)

namespace MyRoot {

    THistogram::THistogram() : TH1D() {}

    THistogram::THistogram(const char* name, const char* title, Int_t nbins, Double_t xmin, Double_t xmax)
        : TH1D(name, title, nbins, xmin, xmax) {}

    THistogram::~THistogram() {}

    Int_t THistogram::Fill(double value) {
        
        int N = (int) TH1D::GetEntries();
        if(TH1D::GetEntries() > 0 && N % 1000 == 0) {
            std::cout << "THistogram: " << TH1D::GetEntries() << " entries" << std::endl;
        }
        
        return TH1D::Fill(value);
    }
    
} // namespace MyRoot

